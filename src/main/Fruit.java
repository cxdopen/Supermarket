import java.math.BigDecimal;

// 水果类
class Fruit {
    private String name; // 水果名称
    private BigDecimal pricePerKg; // 单价（元/斤）

    // 构造函数
    public Fruit(String name, BigDecimal pricePerKg) {
        this.name = name;
        this.pricePerKg = pricePerKg;
    }

    // 获取水果名称
    public String getName() {
        return name;
    }

    // 获取单价
    public BigDecimal getPricePerKg() {
        return pricePerKg;
    }
}
