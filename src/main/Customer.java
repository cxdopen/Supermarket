// 顾客类
public class Customer {
    private String name; // 顾客名称
    private int apples; // 苹果数量（斤）
    private int strawberries; // 草莓数量（斤）
    private int mangoes; // 芒果数量（斤）

    // 构造函数
    public Customer(String name, int apples, int strawberries, int mangoes) {
        this.name = name;
        this.apples = apples;
        this.strawberries = strawberries;
        this.mangoes = mangoes;
    }

    // 获取苹果数量
    public int getApples() {
        return apples;
    }

    // 获取草莓数量
    public int getStrawberries() {
        return strawberries;
    }

    // 获取芒果数量
    public int getMangoes() {
        return mangoes;
    }

    // 获取顾客名称
    public String getName() {
        return name;
    }
}
