// 主类
public class Application {
    public static void main(String[] args) {
        // 测试数据
        Customer customerA = new Customer("A", 5, 2, 0);
        Customer customerB = new Customer("B", 3, 1, 2);
        Customer customerC = new Customer("C", 4, 3, 1);
        Customer customerD = new Customer("D", 6, 2, 3);

        // 计算总价并输出结果
        System.out.println("Customer A Total Price: $" + Supermarket.calculateTotalPrice(customerA));
        System.out.println("Customer B Total Price: $" + Supermarket.calculateTotalPrice(customerB));
        System.out.println("Customer C Total Price: $" + Supermarket.calculateTotalPrice(customerC));
        System.out.println("Customer D Total Price: $" + Supermarket.calculateTotalPrice(customerD));
    }
}

