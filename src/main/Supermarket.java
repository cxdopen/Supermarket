import java.math.BigDecimal;

// 超市类
class Supermarket {
    // 定义水果的价格
    private static final BigDecimal APPLE_PRICE = new BigDecimal("8.00"); // 苹果价格（元/斤）
    private static final BigDecimal STRAWBERRY_PRICE = new BigDecimal("13.00"); // 草莓价格（元/斤）
    private static final BigDecimal MANGO_PRICE = new BigDecimal("20.00"); // 芒果价格（元/斤）
    private static final BigDecimal STRAWBERRY_DISCOUNT = new BigDecimal("0.8"); // 草莓折扣
    private static final BigDecimal DISCOUNT_THRESHOLD = new BigDecimal("100.00"); // 满减阈值（元）
    private static final BigDecimal DISCOUNT_AMOUNT = new BigDecimal("10.00"); // 满减金额（元）

    // 计算顾客购买的总价
    public static BigDecimal calculateTotalPrice(Customer customer) {
        // 计算每种水果的总价
        BigDecimal totalApplePrice = APPLE_PRICE.multiply(BigDecimal.valueOf(customer.getApples()));
        BigDecimal totalStrawberryPrice = STRAWBERRY_PRICE.multiply(BigDecimal.valueOf(customer.getStrawberries()));
        BigDecimal totalMangoPrice = MANGO_PRICE.multiply(BigDecimal.valueOf(customer.getMangoes()));

        // 计算总价
        BigDecimal totalPrice = totalApplePrice.add(totalStrawberryPrice).add(totalMangoPrice);

        // 如果草莓数量大于0，应用草莓折扣
        if (customer.getStrawberries() > 0 && "C".equals(customer.getName())) {
            BigDecimal strawberryDiscount = totalStrawberryPrice.multiply(STRAWBERRY_DISCOUNT);
            totalPrice = totalPrice.subtract(strawberryDiscount);
        }

        // 如果总价达到满减阈值，应用满减优惠
        if (totalPrice.compareTo(DISCOUNT_THRESHOLD) >= 0 && "D".equals(customer.getName())) {
            totalPrice = totalPrice.subtract(DISCOUNT_AMOUNT);
        }

        return totalPrice;
    }
}

